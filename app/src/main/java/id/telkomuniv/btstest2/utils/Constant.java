package id.telkomuniv.btstest2.utils;

public class Constant {
    public static String APP_VERSION = "1.0";
    public static String PUT_EXTRA_NEWS = "PUT_EXTRA_NEWS";
    public static String PUT_EXTRA_NEWS_LIST = "PUT_EXTRA_NEWS_LIST";


    public static String TOKEN_BTS = "TOKEN_BTS";
    public static String PUT_EXTRA_ID_CHECKLIST = "PUT_EXTRA_ID_CHECKLIST";
    public static String PUT_EXTRA_MODEL_CHECKLIST = "PUT_EXTRA_MODEL_CHECKLIST";


}
