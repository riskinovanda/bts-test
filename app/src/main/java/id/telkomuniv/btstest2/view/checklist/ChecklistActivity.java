package id.telkomuniv.btstest2.view.checklist;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.rey.material.widget.ProgressView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.telkomuniv.btstest2.R;
import id.telkomuniv.btstest2.model.BtsChecklist;
import id.telkomuniv.btstest2.model.BtsReqStoreChecklist;
import id.telkomuniv.btstest2.model.BtsResponseData;
import id.telkomuniv.btstest2.model.BtsResponseDataList;
import id.telkomuniv.btstest2.rest.ApiConfig;
import id.telkomuniv.btstest2.rest.ApiService;
import id.telkomuniv.btstest2.utils.Constant;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChecklistActivity extends AppCompatActivity {

    private String TAG = "ChecklistActivity";

    @BindView(R.id.pv_checklist)
    ProgressView progressView;
    @BindView(R.id.rv_checklist)
    RecyclerView recyclerView;

    private ApiService apiService;
    private List<BtsChecklist> btsChecklistList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checklist);

        ButterKnife.bind(this);
        progressView.bringToFront();
        progressView.setVisibility(View.INVISIBLE);
        apiService = ApiConfig.getApiServiceBts(this);
        getDataChecklist();
    }

    private void getDataChecklist() {
        progressView.setVisibility(View.VISIBLE);
        btsChecklistList = new ArrayList<>();
        Call<BtsResponseDataList<BtsChecklist>> call = apiService.indexChecklist(Constant.TOKEN_BTS);
        call.enqueue(new Callback<BtsResponseDataList<BtsChecklist>>() {
            @Override
            public void onResponse(Call<BtsResponseDataList<BtsChecklist>> call, Response<BtsResponseDataList<BtsChecklist>> response) {
                if (response.isSuccessful()){
                    Log.e(TAG, "response success : " );
                    btsChecklistList = response.body().getData();

                    showData();
                }
                else{
                    Log.e(TAG, "response is not success : " + response.raw());
                    Log.e(TAG, "response is not success : " + response.message());
                    Log.e(TAG, "response is not success : " + response.toString());
                    Log.e(TAG, "response is not success : " + response.code());
                    Log.e(TAG, "response is not success : " + response.errorBody());
                    Log.e(TAG, "response is not success : " + response.headers());
                    showToast(getResources().getString(R.string.failed_to_get_data));
                    progressView.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onFailure(Call<BtsResponseDataList<BtsChecklist>> call, Throwable t) {
                Log.e(TAG, "onFailure : " + t.getMessage());
                showToast(getResources().getString(R.string.there_is_an_error));
                progressView.setVisibility(View.INVISIBLE);
            }
        });
    }

    private void showData() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        ChecklistAdapter adapter = new ChecklistAdapter(this, btsChecklistList);
        recyclerView.setAdapter(adapter);

        progressView.setVisibility(View.INVISIBLE);
    }

    private void showToast(String s){
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.fab_add_checklist) void fabAddOnClick(){
        showDialogAddChecklist();
    }

    private void showDialogAddChecklist() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_add_checklist);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        final EditText etName = dialog.findViewById(R.id.et_name_dialog_add_checklist);
        final Button btnAdd = dialog.findViewById(R.id.btn_add_dialog_add_checklist);

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!etName.getText().toString().isEmpty()){
                    storeDataChecklist(etName.getText().toString());
                    dialog.dismiss();
                }
            }
        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    private void storeDataChecklist(String name) {
        progressView.setVisibility(View.VISIBLE);

        BtsReqStoreChecklist btsReqStoreChecklist = new BtsReqStoreChecklist();
        btsReqStoreChecklist.setName(name);

        Call<BtsResponseData<BtsChecklist>> call = apiService.storeChecklist(Constant.TOKEN_BTS, btsReqStoreChecklist);
        call.enqueue(new Callback<BtsResponseData<BtsChecklist>>() {
            @Override
            public void onResponse(Call<BtsResponseData<BtsChecklist>> call, Response<BtsResponseData<BtsChecklist>> response) {
                if (response.isSuccessful()){
                    Log.e(TAG, "response success : " );

                    getDataChecklist();
                }
                else{
                    Log.e(TAG, "response is not success : " + response.raw());
                    Log.e(TAG, "response is not success : " + response.message());
                    Log.e(TAG, "response is not success : " + response.toString());
                    Log.e(TAG, "response is not success : " + response.code());
                    Log.e(TAG, "response is not success : " + response.errorBody());
                    Log.e(TAG, "response is not success : " + response.headers());
                    showToast(getResources().getString(R.string.failed_to_get_data));
                    progressView.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onFailure(Call<BtsResponseData<BtsChecklist>> call, Throwable t) {
                Log.e(TAG, "onFailure : " + t.getMessage());
                showToast(getResources().getString(R.string.there_is_an_error));
                progressView.setVisibility(View.INVISIBLE);
            }
        });
    }
}
