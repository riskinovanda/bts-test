package id.telkomuniv.btstest2.view.checklist_item;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.rey.material.widget.ProgressView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.telkomuniv.btstest2.R;
import id.telkomuniv.btstest2.model.BtsChecklist;
import id.telkomuniv.btstest2.model.News;
import id.telkomuniv.btstest2.rest.ApiConfig;
import id.telkomuniv.btstest2.rest.ApiService;
import id.telkomuniv.btstest2.utils.Constant;
import id.telkomuniv.btstest2.view.checklist.ChecklistAdapter;

public class ChecklistItemActivity extends AppCompatActivity {

    private String TAG = "ChecklistItemActivity";

    @BindView(R.id.pv_checklist_item)
    ProgressView progressView;
    @BindView(R.id.tv_name_checklist_item)
    TextView tvName;
    @BindView(R.id.rv_checklist_item)
    RecyclerView recyclerView;

    private ApiService apiService;
    private BtsChecklist btsChecklist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checklist_item);

        ButterKnife.bind(this);
        progressView.bringToFront();
        progressView.setVisibility(View.INVISIBLE);
        apiService = ApiConfig.getApiServiceBts(this);
        btsChecklist = (BtsChecklist) getIntent().getSerializableExtra(Constant.PUT_EXTRA_MODEL_CHECKLIST);
        showData();
    }

    private void showData() {
        tvName.setText(btsChecklist.getName());

        if (btsChecklist.getItems()!=null){
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(linearLayoutManager);
            ChecklistItemAdapter adapter = new ChecklistItemAdapter(this, btsChecklist.getItems());
            recyclerView.setAdapter(adapter);
        }
    }
}
