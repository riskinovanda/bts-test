package id.telkomuniv.btstest2.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.rey.material.widget.ProgressView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.telkomuniv.btstest2.R;
import id.telkomuniv.btstest2.model.BtsReqLogin;
import id.telkomuniv.btstest2.model.BtsResponseData;
import id.telkomuniv.btstest2.model.BtsResponseToken;
import id.telkomuniv.btstest2.rest.ApiConfig;
import id.telkomuniv.btstest2.rest.ApiService;
import id.telkomuniv.btstest2.utils.Constant;
import id.telkomuniv.btstest2.view.checklist.ChecklistActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private String TAG = "LoginActivity";

    @BindView(R.id.pv_login)
    ProgressView progressView;
    @BindView(R.id.et_username_login)
    EditText etUsername;
    @BindView(R.id.et_password_login)
    EditText etPassword;

    private ApiService apiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);
        progressView.bringToFront();
        progressView.setVisibility(View.INVISIBLE);
        apiService = ApiConfig.getApiServiceBts(this);
    }

    @OnClick(R.id.btn_submit_login) void btnSubmitOnClick(){
        if (!etUsername.getText().toString().isEmpty() && !etPassword.getText().toString().isEmpty())
            storeDataLogin(etUsername.getText().toString(), etPassword.getText().toString());
    }

    private void storeDataLogin(String username, String passwrod) {
        progressView.setVisibility(View.VISIBLE);

        BtsReqLogin btsReqLogin = new BtsReqLogin();
        btsReqLogin.setUsername(username);
        btsReqLogin.setPassword(passwrod);

        Call<BtsResponseData<BtsResponseToken>> call = apiService.storeLogin(btsReqLogin);
        call.enqueue(new Callback<BtsResponseData<BtsResponseToken>>() {
            @Override
            public void onResponse(Call<BtsResponseData<BtsResponseToken>> call, Response<BtsResponseData<BtsResponseToken>> response) {
                if (response.isSuccessful()){
                    Constant.TOKEN_BTS = "Bearer "+response.body().getData().getToken();

                    Log.e(TAG, "response success : " );
                    Log.e(TAG, "response success token : "+Constant.TOKEN_BTS);

                    showToast("Sukses login");
                    progressView.setVisibility(View.INVISIBLE);
                    startActivity(new Intent(LoginActivity.this, ChecklistActivity.class));
                    finish();
                }
                else{
                    Log.e(TAG, "response is not success : " + response.raw());
                    Log.e(TAG, "response is not success : " + response.message());
                    Log.e(TAG, "response is not success : " + response.toString());
                    Log.e(TAG, "response is not success : " + response.code());
                    Log.e(TAG, "response is not success : " + response.errorBody());
                    Log.e(TAG, "response is not success : " + response.headers());
                    showToast(getResources().getString(R.string.failed_to_get_data));
                    progressView.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onFailure(Call<BtsResponseData<BtsResponseToken>> call, Throwable t) {
                Log.e(TAG, "onFailure : " + t.getMessage());
                showToast(getResources().getString(R.string.there_is_an_error));
                progressView.setVisibility(View.INVISIBLE);
            }
        });
    }

    private void showToast(String s){
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }
}
