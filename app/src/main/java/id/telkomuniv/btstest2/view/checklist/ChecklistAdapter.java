package id.telkomuniv.btstest2.view.checklist;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import id.telkomuniv.btstest2.R;
import id.telkomuniv.btstest2.model.BtsChecklist;
import id.telkomuniv.btstest2.model.Country;
import id.telkomuniv.btstest2.utils.Constant;
import id.telkomuniv.btstest2.view.checklist_item.ChecklistItemActivity;
import io.intercom.android.sdk.models.Card;

public class ChecklistAdapter extends RecyclerView.Adapter<ChecklistAdapter.ViewHolder> {

    Context context;
    List<BtsChecklist> btsChecklistList;

    public ChecklistAdapter(Context context, List<BtsChecklist> btsChecklistList) {
        this.context = context;
        this.btsChecklistList = btsChecklistList;
    }

    @NonNull
    @Override
    public ChecklistAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_checklist, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ChecklistAdapter.ViewHolder viewHolder, final int i) {
        viewHolder.tvName.setText(btsChecklistList.get(i).getName());
        viewHolder.cvItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ChecklistItemActivity.class);
                intent.putExtra(Constant.PUT_EXTRA_MODEL_CHECKLIST, btsChecklistList.get(i));
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return btsChecklistList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CardView cvItem;
        TextView tvName;

        public ViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_name_item_checklist);
            cvItem = itemView.findViewById(R.id.cv_item_checklist);
        }
    }
}
