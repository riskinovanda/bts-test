package id.telkomuniv.btstest2.view.checklist_item;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import id.telkomuniv.btstest2.R;
import id.telkomuniv.btstest2.model.BtsChecklist;
import id.telkomuniv.btstest2.model.BtsChecklistItem;
import id.telkomuniv.btstest2.utils.Constant;
import id.telkomuniv.btstest2.view.checklist.ChecklistAdapter;

public class ChecklistItemAdapter extends RecyclerView.Adapter<ChecklistItemAdapter.ViewHolder> {

    Context context;
    List<BtsChecklistItem> btsChecklistItemList;

    public ChecklistItemAdapter(Context context, List<BtsChecklistItem> btsChecklistItemList) {
        this.context = context;
        this.btsChecklistItemList = btsChecklistItemList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_checklist_item, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        viewHolder.tvName.setText(btsChecklistItemList.get(i).getName());
        viewHolder.cvItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return btsChecklistItemList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CardView cvItem;
        TextView tvName;

        public ViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_name_item_checklist_item);
            cvItem = itemView.findViewById(R.id.cv_item_checklist_item);
        }
    }
}
