package id.telkomuniv.btstest2.rest;

import android.content.Context;

public class ApiConfig {

    public static final String API_URL = "http://covid.rasyid.in";
    public static final String API_URL_TELU = "http://telu.rasyid.in";
    public static final String API_BTS = "http://18.141.178.15:8080";

    public static ApiService getApiService(Context context){
        return ApiClient.getClient(API_URL).create(ApiService.class);
    }

    public static ApiService getApiServiceTelu(Context context){
        return ApiClient.getClient(API_URL_TELU).create(ApiService.class);
    }

    public static ApiService getApiServiceBts(Context context){
        return ApiClient.getClient(API_BTS).create(ApiService.class);
    }

    /*public static String getAuth(Context context) {
        new Prefs.Builder()
                .setContext(context)
                .setMode(android.content.ContextWrapper.MODE_PRIVATE)
                .setPrefsName(context.getPackageName())
                .setUseDefaultSharedPreference(true)
                .build();

        return Prefs.getString(Constant.PREF_TOKEN, "");
    }*/

}
