package id.telkomuniv.btstest2.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TimelineResponse {
    @SerializedName("timeline")
    @Expose
    private List<Timeline> timelines;

    public List<Timeline> getTimelines() {
        return timelines;
    }

    public void setTimelines(List<Timeline> timelines) {
        this.timelines = timelines;
    }
}
