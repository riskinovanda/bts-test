package id.telkomuniv.btstest2.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Timeline {
    @SerializedName("updated_at")
    @Expose
    private String updated_at;

    @SerializedName("date")
    @Expose
    private String date;

    @SerializedName("deaths")
    @Expose
    private int deaths;

    @SerializedName("confirmed")
    @Expose
    private int confirmed;

    @SerializedName("active")
    @Expose
    private int active;

    @SerializedName("recovered")
    @Expose
    private int recovered;

    @SerializedName("new_confirmed")
    @Expose
    private int new_confirmed;

    @SerializedName("new_recovered")
    @Expose
    private int new_recovered;

    @SerializedName("new_deaths")
    @Expose
    private int new_deaths;

    @SerializedName("is_in_progress")
    @Expose
    private boolean is_in_progress;

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getDeaths() {
        return deaths;
    }

    public void setDeaths(int deaths) {
        this.deaths = deaths;
    }

    public int getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(int confirmed) {
        this.confirmed = confirmed;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public int getRecovered() {
        return recovered;
    }

    public void setRecovered(int recovered) {
        this.recovered = recovered;
    }

    public int getNew_confirmed() {
        return new_confirmed;
    }

    public void setNew_confirmed(int new_confirmed) {
        this.new_confirmed = new_confirmed;
    }

    public int getNew_recovered() {
        return new_recovered;
    }

    public void setNew_recovered(int new_recovered) {
        this.new_recovered = new_recovered;
    }

    public int getNew_deaths() {
        return new_deaths;
    }

    public void setNew_deaths(int new_deaths) {
        this.new_deaths = new_deaths;
    }

    public boolean isIs_in_progress() {
        return is_in_progress;
    }

    public void setIs_in_progress(boolean is_in_progress) {
        this.is_in_progress = is_in_progress;
    }
}
