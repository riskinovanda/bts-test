package id.telkomuniv.btstest2.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Province {
    @SerializedName("FID")
    @Expose
    private int FID;

    @SerializedName("Kode_Provi")
    @Expose
    private int Kode_Provi;

    @SerializedName("Provinsi")
    @Expose
    private String Provinsi;

    @SerializedName("Kasus_Posi")
    @Expose
    private int Kasus_Posi;

    @SerializedName("Kasus_Semb")
    @Expose
    private int Kasus_Semb;

    @SerializedName("Kasus_Meni")
    @Expose
    private int Kasus_Meni;

    public int getFID() {
        return FID;
    }

    public void setFID(int FID) {
        this.FID = FID;
    }

    public int getKode_Provi() {
        return Kode_Provi;
    }

    public void setKode_Provi(int kode_Provi) {
        Kode_Provi = kode_Provi;
    }

    public String getProvinsi() {
        return Provinsi;
    }

    public void setProvinsi(String provinsi) {
        Provinsi = provinsi;
    }

    public int getKasus_Posi() {
        return Kasus_Posi;
    }

    public void setKasus_Posi(int kasus_Posi) {
        Kasus_Posi = kasus_Posi;
    }

    public int getKasus_Semb() {
        return Kasus_Semb;
    }

    public void setKasus_Semb(int kasus_Semb) {
        Kasus_Semb = kasus_Semb;
    }

    public int getKasus_Meni() {
        return Kasus_Meni;
    }

    public void setKasus_Meni(int kasus_Meni) {
        Kasus_Meni = kasus_Meni;
    }
}
