package id.telkomuniv.btstest2.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class BtsChecklist implements Serializable {
    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("items")
    @Expose
    private List<BtsChecklistItem> items;

    @SerializedName("checklistCompletionStatus")
    @Expose
    private boolean checklistCompletionStatus;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<BtsChecklistItem> getItems() {
        return items;
    }

    public void setItems(List<BtsChecklistItem> items) {
        this.items = items;
    }

    public boolean isChecklistCompletionStatus() {
        return checklistCompletionStatus;
    }

    public void setChecklistCompletionStatus(boolean checklistCompletionStatus) {
        this.checklistCompletionStatus = checklistCompletionStatus;
    }
}
