package id.telkomuniv.btstest2.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Country {
    @SerializedName("OBJECTID")
    @Expose
    private int OBJECTID;

    @SerializedName("Country_Region")
    @Expose
    private String Country_Region;

    @SerializedName("Last_Update")
    @Expose
    private long Last_Update;

    @SerializedName("Lat")
    @Expose
    private double Lat;

    @SerializedName("Long_")
    @Expose
    private double Long_;

    @SerializedName("Confirmed")
    @Expose
    private long Confirmed;

    @SerializedName("Deaths")
    @Expose
    private long Deaths;

    @SerializedName("Recovered")
    @Expose
    private long Recovered;

    @SerializedName("Active")
    @Expose
    private long Active;

    public int getOBJECTID() {
        return OBJECTID;
    }

    public void setOBJECTID(int OBJECTID) {
        this.OBJECTID = OBJECTID;
    }

    public String getCountry_Region() {
        return Country_Region;
    }

    public void setCountry_Region(String country_Region) {
        Country_Region = country_Region;
    }

    public long getLast_Update() {
        return Last_Update;
    }

    public void setLast_Update(long last_Update) {
        Last_Update = last_Update;
    }

    public double getLat() {
        return Lat;
    }

    public void setLat(double lat) {
        Lat = lat;
    }

    public double getLong_() {
        return Long_;
    }

    public void setLong_(double long_) {
        Long_ = long_;
    }

    public long getConfirmed() {
        return Confirmed;
    }

    public void setConfirmed(long confirmed) {
        Confirmed = confirmed;
    }

    public long getDeaths() {
        return Deaths;
    }

    public void setDeaths(long deaths) {
        Deaths = deaths;
    }

    public long getRecovered() {
        return Recovered;
    }

    public void setRecovered(long recovered) {
        Recovered = recovered;
    }

    public long getActive() {
        return Active;
    }

    public void setActive(long active) {
        Active = active;
    }
}
