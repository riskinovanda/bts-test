package id.telkomuniv.btstest2.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class BtsChecklistItem implements Serializable {
    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("itemCompletionStatus")
    @Expose
    private boolean itemCompletionStatus;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isItemCompletionStatus() {
        return itemCompletionStatus;
    }

    public void setItemCompletionStatus(boolean itemCompletionStatus) {
        this.itemCompletionStatus = itemCompletionStatus;
    }
}
