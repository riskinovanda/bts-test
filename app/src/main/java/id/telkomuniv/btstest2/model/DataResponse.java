package id.telkomuniv.btstest2.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataResponse<Model> {
    @SerializedName("data")
    @Expose
    private Model data;

    public Model getData() {
        return data;
    }

    public void setData(Model data) {
        this.data = data;
    }
}
